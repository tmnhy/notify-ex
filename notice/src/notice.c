#include <pebble.h>

#define KEY_CALLS_COUNT 41

static Window *window;
static TextLayer *text_layer;

static AppSync s_sync;
static uint8_t s_sync_buffer[32];

static Layer *layer;
static GBitmap *bmp_phone;

static char s_count_buffer[4];


static void sync_changed_handler(const uint32_t key, const Tuple *new_tuple,
                                 const Tuple *old_tuple, void *context) {

  snprintf(s_count_buffer, sizeof(s_count_buffer), "%d", (int)new_tuple->value->int32);
  layer_mark_dirty(layer);
}

static void sync_error_handler(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "sync error!");
}

static void layer_update_callback(Layer *me, GContext* ctx) {
  GRect bounds = bmp_phone->bounds;

  graphics_draw_bitmap_in_rect(ctx, bmp_phone, (GRect) { .origin = { 5, 5 }, .size = bounds.size });

  graphics_context_set_text_color(ctx, GColorBlack);
  graphics_draw_text(ctx, s_count_buffer, fonts_get_system_font(FONT_KEY_GOTHIC_24),
                    GRect(42, 5, 36, 36),
                    GTextOverflowModeWordWrap, GTextAlignmentLeft,
                    NULL);
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  layer = layer_create(bounds);
  layer_set_update_proc(layer, layer_update_callback);
  layer_add_child(window_layer, layer);

  bmp_phone = gbitmap_create_with_resource(RESOURCE_ID_PHONE_ICON);
}

static void window_unload(Window *window) {
  gbitmap_destroy(bmp_phone);
}

static void start_sync() {
  app_message_open(app_message_inbox_size_maximum(),
                    app_message_outbox_size_maximum());

  Tuplet initial_values[] = {
    TupletInteger(KEY_CALLS_COUNT, 0),
  };

  app_sync_init(&s_sync, s_sync_buffer, sizeof(s_sync_buffer),
                initial_values, ARRAY_LENGTH(initial_values),
                sync_changed_handler, sync_error_handler, NULL);
}

static void stop_sync() {
  app_sync_deinit(&s_sync);
}

static void init(void) {
  window = window_create();

  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);

  start_sync();
}

static void deinit(void) {
  stop_sync();
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
