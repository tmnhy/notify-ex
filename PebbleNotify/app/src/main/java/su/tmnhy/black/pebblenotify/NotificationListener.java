package su.tmnhy.black.pebblenotify;

import android.app.Notification;
import android.content.SharedPreferences;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

// PebbleKit Android
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import java.util.UUID;

public class NotificationListener extends
        NotificationListenerService {

    // Счетчик пропущенных вызовов
    int missedCallsCount = 0;

    // Хранилище
    SharedPreferences sPref;
    final String UUID_TEXT = "uuid_text";

    // UUID ватчфейса и ключ для синхронизации
    UUID APPS_UUID;
    private static final int CALLS_KEY = 41;


    @Override
    public void onNotificationPosted(
            StatusBarNotification sbn) {
        this.getMissedCalls();
    }

    @Override
    public void onNotificationRemoved(
            StatusBarNotification sbn) {
        this.getMissedCalls();
    }

    void getMissedCalls() {
        int tCount = 0;

        for (StatusBarNotification notif :
                this.getActiveNotifications()) {

            if (notif.getPackageName().equals("com.android.phone")) {
                String extras_text = notif.getNotification().extras.getString(Notification.EXTRA_TEXT);
                if (extras_text.indexOf("Пропущенных вызовов:") != -1) {
                    tCount = Integer.parseInt(extras_text.split(":")[1].trim());
                }
            }

        }

        if (tCount != missedCallsCount) {
            missedCallsCount = tCount;
            this.sendMissedCalls(missedCallsCount);
        }
    }

    String getUUID() {
        sPref = getSharedPreferences("watchfaceUUID", MODE_PRIVATE);
        return sPref.getString(UUID_TEXT, "");
    }

    public void sendMissedCalls(int missedCalls) {
        //APPS_UUID = UUID.fromString("de0bc06b-c1da-45a0-bbaf-6fd3735e8748");
        APPS_UUID = UUID.fromString(this.getUUID());

        PebbleDictionary data = new PebbleDictionary();
        data.addUint32(CALLS_KEY, missedCalls);

        PebbleKit.sendDataToPebble(getApplicationContext(), APPS_UUID, data);
    }

}
