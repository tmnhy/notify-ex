package su.tmnhy.black.pebblenotify;

import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {

    Button btnOk;
    EditText uuid0;
    EditText uuid1;
    EditText uuid2;
    EditText uuid3;
    EditText uuid4;

    SharedPreferences sPref;
    final String UUID_TEXT = "uuid_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uuid0 = (EditText) findViewById(R.id.uuid0);
        uuid1 = (EditText) findViewById(R.id.uuid1);
        uuid2 = (EditText) findViewById(R.id.uuid2);
        uuid3 = (EditText) findViewById(R.id.uuid3);
        uuid4 = (EditText) findViewById(R.id.uuid4);

        loadUUID();

        btnOk = (Button) findViewById(R.id.btnOk);
        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUUID();
                finish();
            }
        };
        btnOk.setOnClickListener(oclBtnOk);
    }

    void saveUUID() {
        sPref = getSharedPreferences("watchfaceUUID", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        String joined = this.join(new String[] {uuid0.getText().toString(),"-",
                                                uuid1.getText().toString(),"-",
                                                uuid2.getText().toString(),"-",
                                                uuid3.getText().toString(),"-",
                                                uuid4.getText().toString()});
        ed.putString(UUID_TEXT, joined);
        ed.commit();
    }

    void loadUUID() {
        sPref = getSharedPreferences("watchfaceUUID", MODE_PRIVATE);
        String uuidText = sPref.getString(UUID_TEXT, "");
        if (uuidText != "") {
            String[] uuid_parts = uuidText.split("-");
            if (uuid_parts.length == 5) {
                uuid0.setText(uuid_parts[0]);
                uuid1.setText(uuid_parts[1]);
                uuid2.setText(uuid_parts[2]);
                uuid3.setText(uuid_parts[3]);
                uuid4.setText(uuid_parts[4]);
            }
        }
    }

    public static String join(String[] fields) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            s.append(fields[i]);
        }
        return s.toString();
    }

}
